

/**
 * PIN layout:
 * - 2   : orange (Enable) 
 * - 3   : red (Clock)
 * - 4   : yellow (Data)
 * - 5V  : white (Power)
 * - GND : brown (Ground)
 * 
**/

#include "nes.h"
#include <Arduino.h>

#define ENABLE 2
#define CLOCK 3
#define DATA 4

void setup()
{
  Serial.begin(9600);
  pinMode(ENABLE, OUTPUT);
  pinMode(CLOCK, OUTPUT);
  pinMode(DATA, INPUT_PULLUP);
}

void loop() {
  char in = input(ENABLE, CLOCK, DATA);
  
  if (AIsPressed(in))
    Serial.print("A ");
  if (BIsPressed(in))
    Serial.print("B ");
  if (StartIsPressed(in))
    Serial.print("Start ");
  if (SelectIsPressed(in))
    Serial.print("Select ");
  if (LeftIsPressed(in))
    Serial.print("Left ");
  if (RightIsPressed(in))
    Serial.print("Right ");
  if (UpIsPressed(in))
    Serial.print("Up ");
  if (DownIsPressed(in))
    Serial.print("Down ");
  Serial.println(".");

  delay(500);
}
