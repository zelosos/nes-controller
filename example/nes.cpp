/**
 *  TODO: - comments and shit 
 *        - do not use the digitalWrite and digitalRead funktion and write to
 *          the registers instead
 * 
 **/

#include "nes.h"
#include <Arduino.h>

char input(char enablePin, char clockPin, char dataPin)
{
  // store values in shift regisgter
  digitalWrite(enablePin, 1);
  digitalWrite(enablePin, 0);

  // read values by shifting them out bit by bit
  char input = 0;
  for (char i=0; i<8; i++)
  {
    if (!digitalRead(dataPin))
      input |= 1 << i;
    digitalWrite(clockPin, 1);
    digitalWrite(clockPin, 0);
  }
  return input;
}

// check the read char from input for buttons
bool AIsPressed(char input)      { return (input & BUTTON_A); }
bool BIsPressed(char input)      { return (input & BUTTON_B); }
bool SelectIsPressed(char input) { return (input & BUTTON_SELECT); }
bool StartIsPressed(char input)  { return (input & BUTTON_START); }
bool UpIsPressed(char input)     { return (input & BUTTON_UP); }
bool DownIsPressed(char input)   { return (input & BUTTON_DOWN); }
bool LeftIsPressed(char input)   { return (input & BUTTON_LEFT); }
bool RightIsPressed(char input)  { return (input & BUTTON_RIGHT); }
