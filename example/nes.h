/**
 * TODO: comments and shit
 * 
 * **/

// bitmasks for buttons
#define BUTTON_A      0b00000001
#define BUTTON_B      0b00000010
#define BUTTON_SELECT 0b00000100
#define BUTTON_START  0b00001000
#define BUTTON_UP     0b00010000
#define BUTTON_DOWN   0b00100000
#define BUTTON_LEFT   0b01000000
#define BUTTON_RIGHT  0b10000000

// read input(EnablePort, ClockPort, DataPort) 
char input(char, char, char);

// check the read byte from input for buttons
bool 
  AIsPressed(char),
  BIsPressed(char),
  SelectIsPressed(char),
  StartIsPressed(char),
  UpIsPressed(char),
  DownIsPressed(char),
  LeftIsPressed(char),
  RightIsPressed(char);
